=== VLSM oefening

image::topologie.png[title=Topologie]

==== Bereken het netwerkmasker

Tel eerst de getallen voor de eindapparaten voor elke afdeling bijelkaar op.
Bereken op basis daarvan het juiste netwerkmasker.

==== Zelf ip adres uitkiezen

Kies binnen de RFC 1918 private adres range een geschikt ip adres uit voor je netwerken.

==== Subnets bepalen

Bepaal voor elke afdeling de juiste gateway op de router.
Geef aan de PC die begint met FH het eerste beschikbare adres binnen je subnet.
Geef aan de PC die begint met LH het laatst beschikbare adres binnen je subnet.

NOTE: Werk ook nu weer met commandoscripts!




