1234 hosts
We hebben 2^11 == 2048 als BS
Netmask = 255.255.248.0
aantal hostbits in octet is 3
2^3 = 8
Blocksize = 8

Netwerk = 192.168.0.0

[%header, cols="3,3,3,3,1"]
|===
|NA|FAH|LAH|BC|CIDR
|192.168.0.0|192.168.0.1|192.168.255.254|192.168.255.255|21
|===


Productie heeft 600 hosts nodig.
Daar past het masker 255.255.252.0 bij. We hebben het derde octet nu dus al nodig! 
Immers dit zijn 2^10 hostbits en in totaal dus 1024 hosts. 
2^9 heeft 512 hosts. Dat is te weinig.

We hebben in het derde octet dus 2 hostbits. Dan krijgen we de blocksize 2^2 = 4

.Productie
[%header, cols="1,3,3,3,3,1"]
|===
|Subnet nr|NA|FAH|LAH|BC|CIDR
|1|192.168.0.0|192.168.0.1|192.168.3.254|192.168.3.255|22
|===

We hebben het eerste subnet klaar. Ons tweede subnet is Kantoor. Daar hebben we 118 hosts nodig.
Nu hebben we in het vierde octet 7 hostbits nodig. Immers 2^7 = 128.
2^6 kan niet. Dan zouden we maar 64 hosts in een subnet kunnen plaatsen dat is te weinig.

We moeten nu vanaf 192.168.4.0 beginnen. De adressen 192.168.0.1 - 192.168.3.255 zijn al bezet door productie.

.Kantoor
[%header, cols="1,3,3,3,3,1"]
|===
|Subnet nr|NA|FAH|LAH|BC|CIDR
|1|192.168.4.0|192.168.4.1|192.168.7.254|192.168.7.255|25
|===

Nu kijken we bij logistiek. Hier zijn 508 eindapparaten nodig.
Dan moeten we in ieder geval 9 hostbits hebben. Immers 2^9= 512.
2^8 is te weinig. We hebben dan maar plaats voor 254 available hosts..
We moeten weer in het derde octet werken. We gebruiken maar 1 hostbit in het derde octet. Dus 2^1 = 2.
We moeten nu vanaf 192.168.8.0 beginnen.

.Logistiek
[%header, cols="1,3,3,3,3,1"]
|===
|Subnet nr|NA|FAH|LAH|BC|CIDR
|1|192.168.8.0|192.168.8.1|192.168.9.254|192.168.9.255|23
|===

Tot slot Administratie. Hier zijn maar 8 hosts nodig.
Je zou zeggen 'Dit kunnen we met 3 hostbits doen. Immers 2^3 = 8'. 
Maar je weet dat we het netwerk en broadcast adres niet mogen toekennen aan een host.
We moeten dus wel voor 4 hostsbits kiezen. 2^4 = 16.
Ook moeten we bij het volgende netwerk beginnen. Dat is 192.168.10.0

.Administratie
[%header, cols="1,3,3,3,3,1"]
|===
|Subnet nr|NA|FAH|LAH|BC|CIDR
|1|192.168.10.0|192.168.10.1|192.168.10.14|192.168.10.15|26
|===

